const { celebrate, Segments, Joi } = require('celebrate');

module.exports = {

  SessionRuleBy() {
    return celebrate({
      [Segments.BODY]: Joi.object().keys({
        id: Joi
          .string()
          .required()
          .regex(/^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{8}$/)
          .message("ID invalido"),
      })
    });
  }

} 