const { celebrate, Segments, Joi } = require('celebrate');

module.exports = {
  authorization() {
    return celebrate({
      [Segments.HEADERS]: Joi.object({
        authorization: Joi
          .string()
          .required()
          .regex(/^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{8}$/)
          .message("authorization é obrigatório")
      }).unknown()
    })
  }
}