const { celebrate, Segments, Joi } = require('celebrate');

module.exports = {

  CreateRuleBy() {
    return celebrate({
      [Segments.BODY]: Joi.object().keys({
        name: Joi.string().required().min(3),
        email: Joi.string().required().email(),
        whatsapp: Joi
          .string()
          .required()
          .regex(/^\s*(\d{2}|\d{0})[-. ]?(\d{5}|\d{4})[-. ]?(\d{4})[-. ]?\s*$/,)
          .message("Numero do WhatsApp é invalido. (11)9 0000-0000")
      })
    });
  }

} 
