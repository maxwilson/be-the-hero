const { celebrate, Segments, Joi } = require('celebrate');

module.exports = {

  CreateRuleBy() {
    return celebrate({
      [Segments.BODY]: Joi.object().keys({
        title: Joi.string().required().min(5),
        description: Joi.string().required().min(10),
        value: Joi.number().required()
      })
    });
  },

  DeleteRuleBy() {
    return celebrate({
      [Segments.PARAMS]: Joi.object().keys({
        value: Joi.number().required()
      })
    });
  },

  PageRuleBy() {
    return celebrate({
      [Segments.QUERY]: Joi.object().keys({
        page: Joi.number()
      })
    });
  }

} 