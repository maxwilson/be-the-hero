const express = require('express');
const routes = express.Router();

/* == Import's controllers == */
const OngController = require('./controllers/OngController');
const IncidentController = require('./controllers/IncidentController');
const ProfileController = require('./controllers/ProfileController');
const SessionController = require('./controllers/SessionController');

/* === Import's Validators === */
const OngValidator = require('./controllers/Middleware/validators/Ong/OngValidator');
const authorization = require('./controllers/Middleware/validators/Authorization');
const SessionValidator = require('./controllers/Middleware/validators/Session/SessionValidator');
const IncidentValidator = require('./controllers/Middleware/validators/Incident/IncidentValidator');


routes.post('/sessions',SessionValidator.SessionRuleBy(),SessionController.store); //ok

routes.get('/ongs', OngController.index);
routes.post('/ongs',OngValidator.CreateRuleBy(),OngController.store); //ok

routes.get('/profile', authorization.authorization(),ProfileController.index); //ok

routes.post('/incidents',
  authorization.authorization(),
  IncidentValidator.CreateRuleBy(),
  IncidentController.store
); //ok

routes.get('/incidents',
  IncidentValidator.PageRuleBy(),
  IncidentController.index
); //ok

routes.delete('/incidents/:id',
 authorization.authorization(),
 IncidentValidator.DeleteRuleBy(),
 IncidentController.destroy
); //ok

module.exports = routes;